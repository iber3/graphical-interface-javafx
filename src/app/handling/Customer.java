package app.handling;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Customer 
{
	private final SimpleStringProperty  ID;
	private final SimpleStringProperty 	firstName;
	private final SimpleStringProperty 	lastName;
	private final SimpleStringProperty 	email;
	private final SimpleStringProperty 	Company;
	private final SimpleStringProperty PostalCode;
	
	public Customer(String sID, String fName, String lName, String grp, String sPostalCode, String mail)
	{
		this.ID = new SimpleStringProperty(sID);
		this.firstName = new SimpleStringProperty(fName);
		this.lastName = new SimpleStringProperty(lName);
		this.email = new SimpleStringProperty(mail);
		this.Company = new SimpleStringProperty(grp);
		this.PostalCode = new SimpleStringProperty(sPostalCode);
	}
	
	public StringProperty idProperty()
	{
		return ID;
	}
	
	public StringProperty firstNameProperty()
	{
		return firstName;
	}
	
	public StringProperty lastNameProperty()
	{
		return lastName;
	}
	
	public StringProperty CompanyProperty()
	{
		return Company;
	}
	
	public StringProperty emailProperty()
	{
		return email;
	}
	
	public StringProperty PostalCodeProperty()
	{
		return PostalCode;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getID()
	{
		return ID.get();
	}
	public String getFirstName()
	{
		return firstName.get();
	}
	
	public String getLastName()
	{
		return lastName.get();
	}
	
	public String getEmail()
	{
		return email.get();
	}
	
	public String getCompany()
	{
		return Company.get();
	}
	
	public String getPostalCode()
	{
		return PostalCode.get();
	}
	
	public void setFirstName(String fName)
	{
		firstName.set(fName);
	}
	
	public void setLastName(String lName)
	{
		lastName.set(lName);
	}
	
	public void setEmail(String mail)
	{
		email.set(mail);
	}
	
	public void setCompany(String grp)
	{
		Company.set(grp);
	}
	
	public void setPostalCode(String sPostalCode)
	{
		PostalCode.set(sPostalCode);
	}
	
	public void setID(String id)
	{
		ID.set(id);
	}
	
	@Override
	public String toString()
	{
		return "name: " + firstNameProperty().getValue() + " ,last name: " + lastNameProperty().getValue() + ")";
	}
	
	
	
	
	
	
	
	
	
	
}
