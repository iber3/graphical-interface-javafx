package app.handling;


import app.handling.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import app.handling.DataModel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;

public class FormView extends BorderPane 
{
	
	final static String source = "customers";
	
	
	
	public static DataModel theModel;
	///===============
	public static Label		labelId;
	public static TextField	fieldId;
	
	public static Label		labelName;
	public static TextField	fieldName;
	
	public static Label		labelLast;
	public static TextField	fieldLast;
	
	public static Label		labelCompany;
	public static TextField	fieldCompany;
	
	public static Label		labelPostalCode;
	public static TextField	fieldPostalCode;
	
	public static Label		labelEmail;
	public static TextField	fieldEmail;
	
	public static Label		console;
	
	public static Button		buttonAdd, buttonEdit, buttonRemove;
	
	public static GridPane	recForm;
		
	ListView<Customer>	listcustomers;
	
	TableView<Customer>	tablecustomers;
	TableColumn<Customer, String> columnId;
	TableColumn<Customer, String> columnName;
	TableColumn<Customer, String> columnLast;
	TableColumn<Customer, String> columnCompany;
	TableColumn<Customer, String> columnPostalCode;
	TableColumn<Customer, String> columnEmail;
	
	
	MenuBar		menuBar;
	MenuItem	menuItExit;
	MenuItem	MenuItAbout;
	
	
	
	
	
	public FormView(DataModel model)
	{
		initForm(model);
	}
	
	
	public void initForm(DataModel model)
	{
		this.setPadding(new Insets(3, 3, 3, 3));
		this.theModel = model;
		
		
		createMenuBar();
		createcustomersList(model);
		createcustomersTableAndMenuBar(model);
		createRecForm(model);
		
		setEventHandlers();
		this.setMargin(tablecustomers, new Insets(0,0,50,0));
	}
	
	
	public void createRecForm(DataModel model)
	{
		VBox vbox = new VBox(10);
		{
			recForm = new GridPane();
			{
				recForm.setAlignment(Pos.TOP_LEFT);
				recForm.setHgap(20);
				recForm.setVgap(10);
				recForm.setPadding(new Insets(25, 25, 25, 25));
				
				labelId = new Label("ID :");
				
				fieldId = new TextField();
				fieldId.setVisible(false);
				fieldId.setEditable(false);
				
				labelName = new Label("Firstname :");
				fieldName = new TextField();
				
				labelLast = new Label("Lastname :");
				fieldLast = new TextField();
				
				labelCompany = new Label("Company :");
				fieldCompany = new TextField();
				
				labelPostalCode = new Label("PostalCode :");
				fieldPostalCode = new TextField();
				
				labelEmail = new Label("Email :");
				fieldEmail = new TextField();
				
				//recForm.add(labelId,        0, 0);
				recForm.add(labelName,		0, 1);
				recForm.add(labelLast,	    0, 2);
				recForm.add(labelCompany,		0, 3);
				recForm.add(labelPostalCode,		0, 4);
				recForm.add(labelEmail,		0, 5);
				
				
				recForm.add(fieldId ,		1, 6, 1, 1);
				recForm.add(fieldName,		1, 1, 1, 1);
				recForm.add(fieldLast,	    1, 2, 1, 1);
				recForm.add(fieldCompany,		1, 3, 1, 1);
				recForm.add(fieldPostalCode,		1, 4, 1, 1);
				recForm.add(fieldEmail,		1, 5, 1, 1);
				recForm.setMinWidth(300);
			}
			vbox.getChildren().add(recForm);
			
			HBox buttonBar = new HBox(10);
			{
				buttonAdd  = new Button("Add");
				buttonEdit = new Button("Edit");
				buttonRemove = new Button("Delete");
				buttonBar.getChildren().addAll(buttonAdd, buttonEdit, buttonRemove);
				buttonBar.setAlignment(Pos.TOP_RIGHT);
			}
			vbox.getChildren().addAll(new Separator(), buttonBar);
		}
		this.setCenter(vbox);
	}
	
	public void createcustomersList(DataModel model)
	{
		listcustomers = new ListView<Customer>();
		listcustomers.setMaxHeight(250);
		{
			listcustomers.setItems(model.getCustomerData());
			
			listcustomers.setCellFactory((list) ->
			{
				return new ListCell<Customer>()
				{
					@Override
					protected void updateItem(Customer item, boolean empty)
					{
						super.updateItem(item, empty);
						
						if (item == null || empty)
							setText(null);
						else
							setText(item.idProperty().getValue() +". " +item.firstNameProperty().getValue()
									+ " " + item.lastNameProperty().getValue());
					}
				};
			});
		}
		listcustomers.setMaxWidth(600);
		this.setRight(listcustomers);
	
	}
	
	public void createcustomersTableAndMenuBar(DataModel model)
	{
		VBox vbox = new VBox(1);
		{
	
		tablecustomers = new TableView<Customer>();
		tablecustomers.setMaxHeight(200);
		tablecustomers.setSelectionModel(null);
		
		{
			columnId		= new TableColumn<>("Id");
			columnId.setMinWidth(30);
			columnId.setMaxWidth(30);
			columnId.setSortable(false);
			
			columnName		= new TableColumn<>("Firstname");	
			columnName.setMinWidth(150);
			columnName.setMaxWidth(150);
			columnName.setSortable(false);
			
			columnLast		= new TableColumn<>("Lastname");
			columnLast.setMinWidth(150);
			columnLast.setMaxWidth(150);
			columnLast.setSortable(false);
			
			columnCompany		= new TableColumn<>("Company");
			columnCompany.setMinWidth(150);
			columnCompany.setMaxWidth(150);
			columnCompany.setSortable(false);
			
			columnPostalCode		= new TableColumn<>("PostalCode");
			columnPostalCode.setMinWidth(150);
			columnPostalCode.setMaxWidth(150);
			columnPostalCode.setSortable(false);
			
			columnEmail		= new TableColumn<>("Email");
			columnEmail.setMinWidth(150);
			columnEmail.setMaxWidth(150);
			columnEmail.setSortable(false);
			
			columnId.setCellValueFactory(new PropertyValueFactory<Customer,String>("id"));
			columnName.setCellValueFactory(new PropertyValueFactory<Customer,String>("firstName"));
			columnLast.setCellValueFactory(new PropertyValueFactory<Customer,String>("lastName"));
			columnCompany.setCellValueFactory(new PropertyValueFactory<Customer,String>("Company"));
			columnPostalCode.setCellValueFactory(new PropertyValueFactory<Customer,String>("PostalCode"));
			columnEmail.setCellValueFactory(new PropertyValueFactory<Customer,String>("email"));
			
			
		}
		tablecustomers.getColumns().addAll(columnId,columnName, columnLast, columnCompany, columnPostalCode, columnEmail);
		tablecustomers.setItems(model.getCustomerData());
		}
		
		menuBar = new MenuBar();
		{
			Menu menuFile = new Menu("File");
			{
				menuItExit	= new MenuItem("Exit");
				menuFile.getItems().addAll(menuItExit);
			}
			
			Menu menuApp = new Menu("App");
			{
				MenuItAbout = new MenuItem("About");
				menuApp.getItems().addAll(MenuItAbout);
				
			}
			menuBar.getMenus().addAll(menuFile,menuApp);
		}
		vbox.getChildren().addAll(menuBar,tablecustomers);
		
		
		
		
		this.setTop(vbox);
	}
	
	
	public void createMenuBar()
	{
		menuBar = new MenuBar();
		{
			Menu menuFile = new Menu("File");
			{
				menuItExit	= new MenuItem("Exit");
				menuFile.getItems().addAll(menuItExit);
			}
			
			Menu menuApp = new Menu("App");
			{
				MenuItAbout = new MenuItem("About");
				menuFile.getItems().addAll(MenuItAbout);
				
			}
			menuBar.getMenus().addAll(menuFile,menuApp);
		}
		
		this.setCenter(menuBar);
	}
	
	
	
	
	
	

	
	public void setEventHandlers()
	{
		
		
		

		
		//================== buttonai
		buttonAdd.setOnAction
		((value)->
		{
			if(!fieldName.getText().isEmpty() &&
			   !fieldLast.getText().isEmpty() &&	
			   !fieldCompany.getText().isEmpty()&&
			   !fieldPostalCode.getText().isEmpty()  &&
			   !fieldEmail.getText().isEmpty() 
			   )
			{
			try {
				
				
				Alert alertConf = new Alert(AlertType.CONFIRMATION);
				alertConf.setTitle("Alert");
				alertConf.setHeaderText("Alert");
				alertConf.setContentText("Are you sure? Add " +fieldName.getText()+ " " +fieldLast.getText()+" ?");
				

				Optional<ButtonType> result = alertConf.showAndWait();
				if (result.get() == ButtonType.OK)
				{
					sendAddValues();
					theModel.getCustomerData().clear();
					theModel.loadDbTable(source);
					clearFields();
					
					
					
					Alert alertInfo = new Alert(AlertType.INFORMATION);
					alertInfo.setHeaderText(null);
					alertInfo.setTitle("New item");
					alertInfo.setContentText("Item added");
					alertInfo.showAndWait();
					
				} 
				else 
				{
				    alertConf.close();
				}
	
			} 
			catch (SQLException e) 
			{
				
				e.printStackTrace();
			}
			}
		}
		);
		
		
		
		buttonRemove.setOnAction
		((value)->
		{
		if(!fieldName.getText().isEmpty() &&
				   !fieldLast.getText().isEmpty() &&	
				   !fieldCompany.getText().isEmpty()&&
				   !fieldPostalCode.getText().isEmpty()  &&
				   !fieldEmail.getText().isEmpty() 
				   )
		{
			try 
			{
				
				
				
				Alert alertConf = new Alert(AlertType.CONFIRMATION);
				alertConf.setTitle("Alert");
				
				alertConf.setContentText("Are you sure? Delete " +fieldName.getText()+ " " +fieldLast.getText()+"?");
				

				Optional<ButtonType> result = alertConf.showAndWait();
				if (result.get() == ButtonType.OK)
				{
					sendDelValues();
					
					theModel.getCustomerData().clear();
					theModel.loadDbTable(source);

					
					
					Alert alertInfo = new Alert(AlertType.INFORMATION);
					alertInfo.setHeaderText(null);
					alertInfo.setTitle("Item");
					alertInfo.setContentText("Item Deleted");
					alertInfo.showAndWait();
				} 
				else 
				{
				    alertConf.close();
				}
	
			} 
			
			catch (SQLException e) 
			{
				
				e.printStackTrace();
			}
		}
		
		
		
		}
		);
		
		buttonEdit.setOnAction
		((value)->
		{
			if(!fieldName.getText().isEmpty() &&
					   !fieldLast.getText().isEmpty() &&	
					   !fieldCompany.getText().isEmpty()&&
					   !fieldPostalCode.getText().isEmpty()  &&
					   !fieldEmail.getText().isEmpty() 
					   )
			{
			try 
			{
				
				
				Alert alertConf = new Alert(AlertType.CONFIRMATION);
				alertConf.setTitle("Alert");
				alertConf.setContentText("Are you sure? Edit " +fieldName.getText()+ " " +fieldLast.getText()+"?");
				

				Optional<ButtonType> result = alertConf.showAndWait();
				if (result.get() == ButtonType.OK)
				{
					sendEditValues();
					theModel.getCustomerData().clear();
					theModel.loadDbTable(source);

					
					
					Alert alertInfo = new Alert(AlertType.INFORMATION);
					alertInfo.setHeaderText(null);
					alertInfo.setTitle("Item");
					alertInfo.setContentText("Item edited");
					alertInfo.showAndWait();
				} 
				else 
				{
				    alertConf.close();
				}
			
			}
			catch (SQLException e) 
			{
				
				e.printStackTrace();
			}
			}
		
		
		}
		);
		
		//================ menu items

		menuItExit.setOnAction((value) ->
		{
			closeApp();
		}); 
		
		
		
		
		
		MenuItAbout.setOnAction((value)->
		{
			about();
		}
		);
		
		
		
		
		
		//==================== listo selection
		
		listcustomers.getSelectionModel().selectedItemProperty().addListener((observable, oldvalue, newValue)->
		{
			showCustomerDetails(newValue);
		});
		
		fieldName.textProperty().addListener((observable, oldValue, newValue) ->
		{
		});
		
		fieldLast.textProperty().addListener((observable, oldValue, newValue) ->
		{
		});
		
		fieldCompany.textProperty().addListener((observable, oldValue, newValue) ->
		{
		});
		
		fieldPostalCode.textProperty().addListener((observable, oldValue, newValue) ->
		{
		});
		
		fieldEmail.textProperty().addListener((observable, oldValue, newValue) ->
		{
		});
		
		
		
	}
	
	
	public void sendAddValues() throws SQLException
	{
		theModel.addNewCustomer(source, theModel.getLastId() + 1, fieldName.getText(), fieldLast.getText(), fieldCompany.getText(), fieldPostalCode.getText(), fieldEmail.getText());
	}
	
	public void sendDelValues() throws SQLException
	{
		theModel.deleteCustomer(source, fieldId.getText());
	}
	
	public void sendEditValues() throws SQLException
	{
		theModel.updateCustomer(source, fieldId.getText(), fieldName.getText(), fieldLast.getText(), fieldCompany.getText(), fieldPostalCode.getText(), fieldEmail.getText());
	}
	
	public void clearFields()
	{
		fieldId.setText("");
		fieldName.setText("");
		fieldLast.setText("");
		fieldCompany.setText("");
		fieldPostalCode.setText("");
		fieldEmail.setText("");
		
	}
	
	public void closeApp()
	{
		Platform.exit();
		System.exit(0);
	}
	
	public void about()
	{
		Alert alert = new Alert(AlertType.INFORMATION);

	}
	
	
	public void showCustomerDetails(Customer customer)
	{
		if (customer != null)
		{
			this.fieldId.setText(customer.idProperty().get());
			this.fieldName.setText(customer.firstNameProperty().get());
			this.fieldLast.setText(customer.lastNameProperty().get());
			this.fieldCompany.setText(customer.CompanyProperty().get());
			this.fieldPostalCode.setText(customer.PostalCodeProperty().get());
			this.fieldEmail.setText(customer.getEmail());

		} else {
			this.fieldId.setText(null);
			this.fieldName.setText(null);
			this.fieldLast.setText(null);		
			this.fieldEmail.setText(null);
			this.fieldPostalCode.setText(null);
			this.fieldCompany.setText(null);
		}
	}
	
	
}
