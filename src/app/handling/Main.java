package app.handling;



import app.handling.DataModel;
import app.handling.FormView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;

public class Main extends Application 
{
	BorderPane		root;
	Scene			scene;
	
	DataModel		theModel = null;
	FormView		theView = null;
	
	
	
	
	@Override
	public void init() throws Exception
	{
		super.init();
		this.theModel = new DataModel(DataModel.Source.SRC_DBASE);
		this.theModel.setData("customers");
	}
	
	
	
	
	
	
	@Override
	public void start(Stage stage) 
	{
		try
		{
			
			root 	= new FormView(getModel());
			scene	= new Scene(root, 1000, 550);
			
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setTitle("Customers");
			stage.setResizable(false);
			stage.setScene(scene);
			stage.show();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
	
	

	public static void main(String[] args) 
	{
		launch(args);
	}
	
	//---------------
	public DataModel getModel()
	{
		return this.theModel;
	}
	public FormView getView()
	{
		return this.theView;
	}
	
	
	
}
