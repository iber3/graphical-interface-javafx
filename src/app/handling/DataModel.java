package app.handling;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//----------------------
import app.handling.Customer;
import app.handling.DataModel;
import app.handling.DataModel.Source;




public class DataModel {

	public static enum Source {SRC_NULL, SRC_DBASE };
	
	private ObservableList<Customer> CustomersList = null;
	private Source					srcType		= Source.SRC_NULL;
	private PreparedStatement pst = null;
	private ResultSet rs = null;
	private int lastId;
	Connection conn = this.connectDatabase();
	
	public DataModel()
	{
		CustomersList = FXCollections.observableArrayList();
	}
	
	public DataModel(Source srcType)
	{
		this();
		this.setSourceType(srcType);
	}
	
	public DataModel(String source, Source srcType)
	{
		this();
		this.setData(source, srcType);
	}
	
	private void setSourceType(Source mSrcType) {
		this.srcType = mSrcType;
	}
	
	public Source getSourceType() {
		return srcType;
	}
	
	public ObservableList<Customer> getCustomerData() {
		return CustomersList;
	}
	
	public void setData(String source, DataModel.Source mSrcType) 
	{
		this.setSourceType(mSrcType);
		this.setData(source);
	}
	
	public void setData(String source) 
	{
		switch (getSourceType())
		{
			case SRC_DBASE:
				this.loadDbTable(source);
				break;
			case SRC_NULL:
				break;
			default:
				break;
		}
	}
	
	public final void loadDbTable(String source)
	{
        final String sql = "SELECT * FROM " + source ;
        
        try (Connection conn = this.connectDatabase();
             Statement  stmt = conn.createStatement();
             ResultSet  rs = stmt.executeQuery(sql) )
        {
            
        	SortedSet<String> tmp_stateset = new TreeSet<>();
            
        	Customer record = null;
            while (rs.next()) 
            {
            	record = new Customer(rs.getString("id"),
            						rs.getString("firstName"),
            						rs.getString("lastName"),
            						rs.getString("company"),
            						rs.getString("PostalCode"),
            						rs.getString("email")
            					  );
            	
            	CustomersList.add(record);					
            	tmp_stateset.add(rs.getString("firstName"));	
            }
			
        } 
        catch (SQLException e) 
        {
            //e.printStackTrace();
        	System.out.println(e.getMessage());
        }
 	
	}
	
	public void addNewCustomer(String source, int id ,String name, String last, String company, String PostalCode, String email) throws SQLException
	{
		
		final String query = "INSERT INTO " +source +"(id,firstName,lastName,company,PostalCode,email) VALUES(?,?,?,?,?,?)";
		pst = conn.prepareStatement(query);
		
		
		pst.setInt(1, id);
		pst.setString(2, name);
		pst.setString(3, last);
		pst.setString(4, company);
		pst.setString(5, PostalCode);
		pst.setString(6, email);
		
		pst.execute();
		pst.close();
	}
	
	public void deleteCustomer(String source, String id)
	{
		
		try
		{
			final String query = "delete from "+ source + " where id = ?";
			pst = conn.prepareStatement(query);
			pst.setString(1, id);
			
			pst.executeUpdate();
			pst.close();
		}
		catch(SQLException ex)
		{
			Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void updateCustomer(String source,String id,String name, String last, String company, String PostalCode, String email)
	{
		try
		{
			final String query = "update " +source+ " set firstName = ?, lastName = ?, company = ?, PostalCode = ?, email = ? where id = ?";
			
			pst = conn.prepareStatement(query);
			pst.setString(1, name);
			pst.setString(2, last);
			pst.setString(3, Company);
			pst.setString(4, PostalCode);
			pst.setString(5, email);
			pst.setString(6, id);
			
			pst.execute();
			pst.close();
			
			
			
		}
		catch(SQLException ex)
		{
			Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	
	
	
	public int getLastId()
	{
		
		try
		{
			
			String query = "SELECT id FROM Customers ORDER BY id DESC LIMIT 1";
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			
			lastId = rs.getInt("id");
		
			
		}
		catch(Exception e2)
		{
			System.err.println(e2);
		}
		return lastId;
	}
	
	
	
	
	
    private final Connection connectDatabase() 
    {
        // SQLite connection string
        String 		url  = "jdbc:sqlite:./data/dbs/chinook.db";
        Connection 	conn = null;
        try 
        {
            conn = DriverManager.getConnection(url);
        } 
        catch (SQLException e) 
        {
            System.out.println(e.getMessage());
        }
        return conn;
    }
	
    public void close()
    {
		switch (getSourceType())
		{
			case SRC_DBASE:
				// TODO: Do something with the database
				break;
			case SRC_NULL:
				// TODO: Do nothing
				break;
			default:
				// TODO: Do nothing
				break;
		}
    	
	
	
    }
}	
